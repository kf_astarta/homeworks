import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;

import java.util.List;


/**
 * Created by astarta on 12.04.2017.
 */
public class Selenium {
    public static void main(String... args) throws InterruptedException {
        String property = System.getProperty("user.dir") + "/driver/chromedriver.exe";
        System.setProperty("webdriver.chrome.driver", property);

        WebDriver driver = new ChromeDriver();
        driver.get("http://prestashop-automation.qatestlab.com.ua/admin147ajyvk0/");
        WebElement searchField = driver.findElement(By.id("email"));
        searchField.sendKeys("webinar.test@gmail.com");
        searchField = driver.findElement(By.id("passwd"));
        searchField.sendKeys("Xcg7299bnSmMuRLp9ITw");
        WebElement searchButton = driver.findElement(By.name("submitLogin"));
        searchButton.click();
        Thread.sleep(4000);
        WebElement searchUserPic = driver.findElement(By.className("employee_avatar_small"));
        searchUserPic.click();
        searchButton = driver.findElement(By.id("header_logout"));
        searchButton.click();
        
        driver.quit();
    }

}
